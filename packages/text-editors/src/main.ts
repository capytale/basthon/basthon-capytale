// tinymce is dynamically imported when needed
import { PromiseDelegate } from "promise-delegate";

/**
 * A class to track created (rich) text editors.
 */
class TextEditors {
  /**
   * Dict for created editors.
   */
  private _editors: { [key: string]: TextEditor } = {};

  public constructor() {}

  /**
   * Register an editor.
   */
  public register(id: string, editor: TextEditor) {
    this._editors[id] = editor;
  }

  /**
   * Get an aditor from its id.
   */
  public get(id: string) {
    return this._editors[id];
  }

  /**
   * Ensure all editors are loaded.
   */
  public async allLoad() {
    const promises = Object.values(this._editors).map((e) => e.load());
    return await Promise.all(promises);
  }

  /**
   * Set theme to dark/light..
   */
  public async setTheme(theme: "dark" | "light") {
    // ensure all editors are loaded
    await this.allLoad();

    // recreate all editors
    const editors = this._editors;
    this._editors = {};

    for (const id in editors) {
      const editor = editors[id];
      // copying if theme is OK
      if (theme === editor.theme) this._editors[id] = editor;
      else {
        const tinymce = await import("./tinymce-wraps");
        await tinymce.tinymce.remove("#" + id);
        new TextEditor(id, theme, editor.mathjax);
      }
    }
    // ensure loading finished
    await this.allLoad();
  }
}

export const textEditors = new TextEditors();

/**
 * Object wrapper around TinyMCE API.
 */
export class TextEditor {
  public readonly theme: string;
  private _editor: any;
  private _load: Promise<void>;
  public readonly mathjax: boolean;

  public constructor(id: string, theme: string, mathjax: boolean = true) {
    this.theme = theme;
    this.mathjax = mathjax;
    textEditors.register(id, this);
    this._load = (async () => {
      const tinymce = await import("./tinymce-wraps");
      const tinymceLoad = new PromiseDelegate<void>();
      const params: any = {
        inline: true,
        content_css: false,
        selector: "#" + id,
        theme_advanced_resizing: false,
        menubar: false,
        statusbar: false,
        branding: false,
        setup: (editor: any) => {
          this._editor = editor;
          editor.on("init", tinymceLoad.resolve.bind(tinymceLoad));
          editor.on("error", tinymceLoad.reject.bind(tinymceLoad));
        },
        toolbar:
          "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | code",
        plugins: "code",
      };
      if (mathjax) {
        params.external_plugins = { mathjax: tinymce.mathjaxPlugin };
        params.mathjax = {
          lib: "https://cdn.jsdelivr.net/npm/mathjax@3.2.0/es5/tex-mml-chtml.js",
        };
        params.toolbar += " | mathjax";
      }
      if (theme === "dark") {
        params.skin_url = tinymce.darkSkin;
      }
      tinymce.tinymce.init(params);
      // wait for tinymce loading
      await tinymceLoad.promise;
    })();
  }

  /**
   * Promise to ensure editor is loaded.
   */
  public async load() {
    return await this._load;
  }

  /**
   * Get the editor's content.
   */
  public content() {
    return this._editor?.getContent();
  }

  /**
   * Set the editor's content.
   */
  public setContent(content: string) {
    this._editor?.setContent(content);
  }

  /**
   * Connect callback to editor event.
   */
  public addEventListener() {
    this._editor.on.apply(this._editor, arguments);
  }
}
