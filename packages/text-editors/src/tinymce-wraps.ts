import _tinymce from "tinymce";

import "tinymce/icons/default";
import "tinymce/themes/silver";
import "tinymce/skins/ui/oxide/skin.css";
import "tinymce/plugins/code/plugin";
import "tinymce/models/dom";
// the resourceQuery /path/ should be defined in webpack.
//@ts-ignore
import _darkSkin from "./tinymce/skins/ui/basthon-dark/skin.css?path";
//@ts-ignore
//import _darkContent from "./tinymce/skins/content/basthon-dark/content.css?path";
//@ts-ignore
import _mathjaxPlugin from "@dimakorotkov/tinymce-mathjax/plugin.min?path";

export const tinymce = _tinymce;
export const darkSkin = _darkSkin;
//export const darkContent = _darkContent;
export const mathjaxPlugin = _mathjaxPlugin;
