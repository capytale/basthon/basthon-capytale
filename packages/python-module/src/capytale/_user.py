"""Access user ids."""

import js
from typing import Union

__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"


__all__ = [
    "me",
    "student",
]


def me() -> Union[None, int]:
    """Return the current user id or None if it can't be accessed."""
    try:
        return js.window._capytaleTestsProxy.capytale.entity.me
    except Exception:
        return None


def student() -> Union[None, int]:
    """Return the current student id or None if it can't be accessed."""
    try:
        return js.window._capytaleTestsProxy.capytale.entity.student
    except Exception:
        return None
