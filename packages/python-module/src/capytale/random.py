"""Coherent randomness across multiple activities."""

import random
from . import _user

__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"

__all__ = [
    "user_seed",
]


def user_seed() -> None:
    """Get a user attached seed to pass to Python's
    `random.seed`. This is useful to reproduce the same randomness
    across multiple activities (e.g. correction).

    It takes care of the user status in the sens that when a teacher
    runs the student's activity, it gets the student's seed.

    Usage::

            import random
            from capytale.random import user_seed

            random.seed(a=user_seed())
    """
    student = _user.student()
    if student is not None:
        return student
    me = _user.me()
    if me is not None:
        return me
    return random.seed()
