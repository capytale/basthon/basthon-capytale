"""The Capytale module should be used only with the Basthon's Python 3
kernel on the Capytale server.

Look at the sub-modules' documentation for specific details."""

__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"


__all__ = []
