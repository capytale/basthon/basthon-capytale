"""All the machinery to manage autoevalutation in Capytale."""

from basthon.autoeval import (
    equality,
    validationclass,
    Validate,
    ValidateSequences,
    ValidateVariables,
    ValidateFunction,
    ValidateFunctionPretty,
)

__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"


__all__ = [
    "equality",
    "validationclass",
    "Validate",
    "ValidateSequences",
    "ValidateVariables",
    "ValidateFunction",
    "ValidateFunctionPretty",
]
