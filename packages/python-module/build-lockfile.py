#!/usr/bin/env python3

import json
from pathlib import Path
import hashlib

node_modules = Path(__file__).parents[2] / "node_modules"

# get original lockfile from basthon kernel
with open(
    node_modules / "@basthon" / "kernel-python3" / "lib" / "dist" / "repodata.json"
) as j:
    repodata = json.load(j)


def update_lockfile(packages):
    """Update the 'packages' item of lockfile."""
    repodata["packages"] = repodata["packages"] | packages


# compute sha256 for capytale.tar
with open(Path("lib") / "dist" / "capytale.tar", "rb") as f:
    sha256 = hashlib.sha256(f.read()).hexdigest()

# lockfile basthon's part
update_lockfile(
    {
        "capytale": {
            "name": "capytale",
            "version": "0.0.1",
            "file_name": "{basthonRoot}/capytale.tar",
            "install_dir": "site",
            "sha256": sha256,
            "depends": [],
            "imports": ["capytale"],
        }
    }
)

# writing repodata.json
with open("lib/dist/repodata.json", "w") as f:
    json.dump(repodata, f)
