import { Capytale } from "@basthon-capytale/core";
import { Storage, CheckpointsManager } from "@basthon/checkpoints";

/**
 * A class to manage multiple checkpoints, per activity and per user.
 */
export class ActivityCheckpoints {
  protected readonly _capytale: Capytale;
  // for each activity-id.user-id, we store the last viewed timestamp.
  protected readonly _activities: Storage<number>;
  // the name of the _activities DB
  protected readonly _storageName: string;
  // the DB key where timestamp is stored (also used to retreive
  // the CheckpointsManager DB).
  protected readonly _key: string;
  // timeout to remove old checkpoints (in milliseconds)
  protected readonly _cleanStartsAfter: number = 60 * 1000;
  // age of removed checkpoints (in milliseconds)
  protected readonly _maxAge: number = 30 * 1000 * 60 * 60 * 24;
  // the checkpoint manager DB
  protected _checkpoints?: CheckpointsManager<string>;
  // maximum number of checkpoints for a couple (activity, user)
  protected readonly _maxCheckpoints = 10;

  public constructor(capytale: Capytale, language: string) {
    this._capytale = capytale;
    const frontend: string = this._capytale.frontend;
    this._storageName = `basthon-capytale-${frontend}.${language}`;
    this._activities = new Storage<number>(this._storageName);
    this._key = `${this._capytale.entityId}.${this._capytale.get("me")}`;
  }

  /**
   * Get the checkpoints manager DB.
   */
  public get checkpoints(): CheckpointsManager<string> | undefined {
    return this._checkpoints;
  }

  /**
   * Initialise the checkpoints system.
   */
  public async init(): Promise<void> {
    await this._activities.ready();
    // update the current timestamp
    const timestamp: number = Date.now();
    this._activities.set(this._key, timestamp);
    // Get the CheckpointsManager table
    this._checkpoints = new CheckpointsManager<string>(
      this._checkpointsName(this._key),
      this._maxCheckpoints
    );
    await this._checkpoints?.ready();
    // remove old checkpoints
    window.setTimeout(
      this._removeOldCheckpoints.bind(this),
      this._cleanStartsAfter
    );
  }

  /**
   * Remove checkpoints not visited since _maxAge.
   */
  private async _removeOldCheckpoints(): Promise<void> {
    const keys = await this._activities.keys();
    if (keys == null) return;
    for (const key of keys) {
      const timestamp = await this._activities.get(key);
      if (Date.now() - (timestamp ?? 0) > this._maxAge) {
        // remove the refernce
        await this._activities.remove(key);
        // remove the checkpoints table
        await CheckpointsManager.drop(this._checkpointsName(key));
      }
    }
  }

  /**
   * Get a checkpoint table name from its key.
   */
  private _checkpointsName(key: string): string {
    return `${this._storageName}.${key}`;
  }
}
