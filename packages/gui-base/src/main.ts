import { GUIBase } from "@basthon/gui-base";
import { Storage } from "@basthon/checkpoints";

import {
  Capytale,
  CapytaleException,
  ASSIGNMENT_STATE,
} from "@basthon-capytale/core";
import { TextEditor, textEditors } from "@basthon-capytale/text-editors";
import { ActivityCheckpoints } from "./activity-checkpoints";
import { sanitize } from "./sanitizer";
import DateDiff from "./date-diff";

declare global {
  interface Window {
    MathJax?: any;
    _capytaleTestsProxy?: { [key: string]: any };
  }
}

/**
 * The base class for building a GUI using Capytale.
 */
export class GUICapytale {
  protected _gui: GUIBase | null = null;
  protected readonly _capytale: Capytale;
  // per activity checkpoints
  protected _activityCheckpoints?: ActivityCheckpoints;
  // per activity storage
  protected _activityStorage?: Storage<any>;

  public constructor(capytale: Capytale, gui: GUIBase) {
    this._capytale = capytale;
    this._gui = gui;
    // tell Capytale who is the kernel
    this._gui.kernelLoader
      .kernelAvailable()
      .then(this._capytale.setKernel.bind(this._capytale));
    window._capytaleTestsProxy = {
      gui: gui,
      capytale: this._capytale,
      textEditors: textEditors,
    };
  }

  /**
   * Capytale object getter.
   */
  public get capytale() {
    return this._capytale;
  }

  /**
   * DataManager getter.
   */
  public get dataManager() {
    return this.capytale.dataManager;
  }

  /**
   * Initialize the Capytale part of the GUI.
   * Return boolean to know if the GUI is in Capytale mode.
   */
  public async init() {
    if (!this.capytale.capytaleMode()) return false;

    const init = this._gui?.initCaller.bind(this._gui);
    if (init == null) return;

    try {
      // loading Capytale entity
      await init(
        this.capytale.loadEntity.bind(this.capytale),
        "Récupération de l'entité Capytale...",
        false,
      );
      // checking that entity request confirms Capytale mode
      if (!this.capytale.capytaleMode()) return false;
      console.log("Mode de consultation : " + this.capytale.capytaleMode());
      console.log(
        "Statut de la copie : " + this.capytale.assignmentStateText(),
      );

      // setup the UI
      await this.setupUI();

      return true;
    } catch (error: any) {
      this._gui?.notifyError(error as Error);
      return false;
    }
  }

  /**
   * Load files attached to entities.
   */
  public async loadAttachedFiles(): Promise<void> {
    const init = this._gui?.initCaller.bind(this._gui);
    if (init == null) return;
    // load files (if any)
    await init(
      this.capytale.loadFilesFromEntity.bind(this.capytale),
      "Chargement des fichiers auxiliaires...",
      true,
    );
  }

  /**
   * Setup user interface.
   */
  protected async setupUI() {
    const init = this._gui?.initCaller.bind(this._gui);
    if (init == null) return;

    // get checkpoints
    this._activityCheckpoints = new ActivityCheckpoints(
      this.capytale,
      this._gui?.language ?? "",
    );
    await this._activityCheckpoints.init();

    const dbActivityName = `capytale-${this.capytale.frontend}.${
      this._gui?.language
    }.${this.capytale.entityId}.${this.capytale.get("me")}`;
    this._activityStorage = new Storage<any>(dbActivityName);

    // decide if we load from storage
    let cpApproved;
    if (this._activityCheckpoints.checkpoints != null) {
      // we should erase Basthon's checkpoints with our
      this._gui?.setCheckpointsManager(this._activityCheckpoints.checkpoints);
      cpApproved = await this._gui?.lastBackupValid();
    }

    // load from storage or from entity?
    let loadFromEntity: boolean = true;
    if (cpApproved != null && !cpApproved) {
      // oops, something goes wrong and the previous save
      // operation does not succeeded (infinite loop)
      const content = await this._gui?.loadFromStorage(false); // do not set content
      if (content != null) {
        // loadFromStorage can return null
        this._gui?.setContent(content);
        this.setDirty(true);
        loadFromEntity = false;
      }
    }
    if (loadFromEntity) {
      // sadly, mess in script/console term
      let frontend = this.capytale.frontend;
      if (frontend === "console") frontend = "script";

      // set content
      let content = this.capytale.get(`student-${frontend}`);
      //!\ side effetct: empty script comes back to activity one
      if (!content) content = this.capytale.get(`activity-${frontend}`);
      if (content != null) this._gui?.setContent(content);
      this.setDirty(false);
    }
    // dataManager access main content via getValue/setContent
    // through the editor global variable, so fake it.
    // (historically, this comes from Console)
    window._capytaleEditor = {
      getValue: () => this._gui?.content(),
      setContent: (content: string) => this._gui?.setContent(content),
    };

    // loading header (title, description, ...)
    await init(this.initHeader.bind(this), "Chargement de l'entête...", false);

    // set document title to entity's title.
    try {
      document.title = this.capytale.get("title");
    } catch (e) {}

    // go back
    const backButton = document.getElementById(
      "capytale-goback",
    ) as HTMLLinkElement;
    if (backButton != null) {
      backButton.href = this.capytale.returnURL();
    }

    // setup countdown
    if (
      this.capytale.capytaleMode() === "student" &&
      this.capytale.timeRangeEnabled()
    ) {
      const callback = () => {
        const notification = document.getElementById(
          "capytale-notification-area",
        );
        const noTimerange = () => {
          if (notification != null) notification.innerText = "";
          this.disableSaveButton();
          const ongoingButton = document.getElementById(
            "capytale-assignment-state-student-ongoing",
          );
          if (ongoingButton != null) ongoingButton.style.display = "none";
        };
        if (this.capytale.isReadOnly()) {
          noTimerange();
          return;
        }
        // TODO: if there is no notification element, noTimerange will not be called
        if (notification == null) return;

        const { end } = this.capytale.timeRange();
        const now = new Date(this.capytale.now());
        const { diff, format, timeout } = new DateDiff(end, now).fancy();
        if (diff <= 0) {
          noTimerange();
          return;
        }
        const formats = {
          years: "an",
          months: "mois",
          days: "j",
          hours: "h",
          minutes: "min",
          seconds: "s",
          milliseconds: "ms",
        };
        notification.innerText = `Reste ${Math.floor(diff)}+ ${
          (formats as any)[format]
        }.`;
        if (["seconds", "milliseconds"].includes(format))
          notification.classList.add("alert-danger");
        setTimeout(callback, timeout + 10);
      };
      callback();
    }

    // submit the assignment
    let button = document.getElementById(
      "capytale-assignment-state-student-ongoing",
    );
    if (button != null) {
      button.addEventListener("click", () => {
        if (this.saySaveBeforeChangeAssignmentState()) return;
        this._gui?.confirm(
          "Confirmer le rendu du travail",
          "Êtes-vous sûr de vouloir rendre votre travail ? Il ne sera plus modifiable.",
          "Confirmer",
          () => this.setAssignmentState("returned"),
          "Annuler",
          () => {},
        );
      });
    }

    // force the assignment submission
    button = document.getElementById(
      "capytale-assignment-state-teacher-review-ongoing",
    );
    if (button != null) {
      button.addEventListener("click", () => {
        this._gui?.confirm(
          "Forcer le rendu du travail",
          "Êtes-vous sûr de vouloir imposer le rendu de ce travail ? L'élève ne pourra plus le modifier.",
          "Confirmer",
          () => this.setAssignmentState("returned"),
          "Annuler",
          () => {},
        );
      });
    }

    // set corrected
    button = document.getElementById(
      "capytale-assignment-state-teacher-review-returned",
    );
    if (button != null) {
      button.addEventListener("click", () => {
        this._gui?.select(
          "Modifier l'état de l'activité",
          "L'élève a rendu son travail. Que voulez-vous faire ?",
          [
            {
              text: `Indiquer que ce travail a été corrigé`,
              handler: () => {
                if (!this.saySaveBeforeChangeAssignmentState())
                  this.setAssignmentState("corrected");
              },
            },
            {
              text: `Revenir au statut "travail non rendu" (l'élève pourra de nouveau modifier son travail)`,
              handler: () => this.setAssignmentState("ongoing"),
            },
          ],
          "Annuler",
          () => {},
        );
      });
    }

    // modify corrected
    button = document.getElementById(
      "capytale-assignment-state-teacher-review-corrected",
    );
    if (button != null) {
      button.addEventListener("click", () => {
        this._gui?.select(
          "Modifier l'état de l'activité",
          "Ce travail est actuellement indiqué comme étant corrigé. Que voulez-vous faire ?",
          [
            {
              text: `Revenir au statut "travail rendu"`,
              handler: () => this.setAssignmentState("returned"),
            },
            {
              text: `Revenir au statut "travail non rendu" (l'élève pourra de nouveau modifier son travail)`,
              handler: () => this.setAssignmentState("ongoing"),
            },
          ],
          "Annuler",
          () => {},
        );
      });
    }

    // link to teacher share
    const linkShare = document.getElementById(
      "capytale-share-with-teacher-link",
    ) as HTMLLinkElement;
    if (linkShare) {
      linkShare.href = this.capytale.editURL();
    }

    // copy student code
    button = document.getElementById("capytale-student-code");
    const code = this.capytale.get("student-code");
    if (button != null && code)
      button.parentNode?.addEventListener("click", () =>
        GUIBase.copyToClipboard(code),
      );
  }

  /**
   * Initialize text field in header.
   */
  protected initHeaderTextContent(id: string) {
    const elem = this.dataManager.domElement(id);
    if (elem == null) return;
    const editable = this.dataManager.editable(id);
    let content = this.capytale.get(id) ?? "";
    if (this.dataManager.editor(id) !== "rich") {
      // textarea -> !readonly | div -> contenteditable
      if (editable)
        if (elem.tagName === "TEXTAREA")
          (elem as HTMLTextAreaElement).readOnly = false;
        else elem.contentEditable = "true";
    }
    if (elem.tagName === "TEXTAREA") {
      const _elem = elem as HTMLTextAreaElement;
      _elem.value = content;
    } else {
      if (!this.dataManager.safeHTML(id)) content = sanitize(content) as string;
      elem.innerHTML = content;
    }
  }

  /**
   * Initialize rich editor.
   */
  protected async initHeaderRichEditor(id: string) {
    const editable = this.dataManager.editable(id);
    if (editable && this.dataManager.editor(id) === "rich") {
      const theme: string = (await this._gui?.theme()) ?? "light";
      const editor = new TextEditor(
        this.dataManager.domId(id),
        theme,
        this.capytale.frontend !== "notebook", // no mathjax in notebook
      );
      await editor.load();
    }
  }

  /**
   * Initialize checkboxes.
   */
  protected initCheckbox(id: string) {
    const checkbox = this.dataManager.domElement(id) as HTMLInputElement;
    checkbox.checked = Boolean(this.capytale.get(id));
    // disabling if not editable
    if (!this.dataManager.editable(id)) {
      const checkbox = this.dataManager.domElement(id) as HTMLInputElement;
      checkbox.disabled = true;
      // disabling grey hover effect
      const parent = checkbox?.parentElement;
      const label = parent?.getElementsByTagName("label")[0];
      label?.classList?.remove("greyhover");
    }
  }

  /**
   * Loading content in Capytale header.
   */
  protected async initHeader() {
    let promises = [];
    for (let id of this.dataManager.headerDatas()) {
      if (this.dataManager.visible(id)) {
        switch (this.dataManager.type(id)) {
          case "text":
            this.initHeaderTextContent(id);
            promises.push(this.initHeaderRichEditor(id));
            break;
          case "checkbox":
            this.initCheckbox(id);
            break;
        }
      } else {
        // hide or remove element
        const elem = this.dataManager.domElement(id);
        if (elem == null) continue;
        if (this.dataManager.keepIfHidden(id)) elem.style.display = "none";
        else elem.parentNode?.removeChild(elem);
      }
    }

    // disabling save button in read-only mode
    if (this.capytale.isReadOnly()) this.disableSaveButton();

    // prevent newline during title edition
    const title = document.getElementById("capytale-title");
    title?.addEventListener("keydown", (event) => {
      document.title = title.innerText;
      if (["Enter", "NumpadEnter"].includes(event.code)) {
        event.preventDefault();
        title.blur();
      }
      // this prevents command_mode shortcuts
      // this is drastic and it may be smarter to use
      // gui.notebook.keyboard_manager.edit_mode()/command_mode()
      event.stopPropagation();
    });

    // displaying result
    this.displayHeader();

    await Promise.all(promises);

    // rendering eventual latex in header
    (async () => {
      try {
        if (window.MathJax.typeset) window.MathJax.typeset();
        else window.MathJax.Hub.Queue(["Typeset", window.MathJax.Hub]);
      } catch (e) {}
    })();
  }

  /**
   * Disabling the save button.
   */
  protected disableSaveButton() {
    const saveButton = document.getElementById(
      "capytale-save",
    ) as HTMLButtonElement;
    if (saveButton == null) return;
    saveButton.disabled = true;
  }

  /**
   * Enabling the save button.
   */
  protected enableSaveButton() {
    const saveButton = document.getElementById(
      "capytale-save",
    ) as HTMLButtonElement;
    if (saveButton == null) return;
    saveButton.disabled = false;
  }

  /**
   * Show Capytale's header.
   */
  protected displayHeader() {
    for (let e of document.getElementsByClassName("capytale-hide")) {
      (e as HTMLElement).style.display = "block";
    }
  }

  /**
   * Save all data depending on mode.
   */
  public async save(only?: string[]) {
    // force checkpoint backup
    let localBackupSuccess = undefined;
    try {
      await this._gui?.backup(false);
      localBackupSuccess = true;
    } catch (error: any) {
      localBackupSuccess = false;
    }
    const data = this.dataManager.editableContents();
    if (only != null) for (let d in data) if (!only.includes(d)) delete data[d];
    try {
      await this.capytale.save(data);
    } catch (error: any) {
      let message = "Impossible d'enregistrer les informations dans Capytale.";
      let nested: string = (error as Error).message;
      if (nested == null)
        nested = (error as PromiseRejectionEvent)?.reason?.message;
      if (nested == null) nested = error.toString();
      if (nested) message += "\n" + nested;
      throw new CapytaleException(message);
    }
    if (localBackupSuccess) await this._gui?.validateBackup();
    this._gui?.info(
      "Enregistré avec succès.",
      "Les informations ont bien été enregistrées dans Capytale.",
    );
    return data;
  }

  /**
   * Download content as file using activity title.
   */
  public async downloadContent() {
    let title: string | undefined =
      document.getElementById("capytale-title")?.innerText;
    //@ts-ignore
    const ext = this._gui?._contentFilename.split(".").pop();
    if (title != null) {
      // see https://github.com/sindresorhus/filenamify/issues/26
      //@ts-ignore
      const filenamify = await import("filenamify/browser");
      title = filenamify.default(title, { replacement: "-" }) || undefined;
      if (title != null) title = `${title}.${ext}`;
    }
    this._gui?.download(title);
  }

  /**
   * Is a save needed?
   */
  public dirty(): boolean {
    return false;
  }

  /**
   * Force dirty/not dirty
   */
  public setDirty(dirty: boolean): void {}

  /**
   * In student mode, roll main content back to teacher's one or choose a checkpoint.
   * In all other mode: roolback to a checkpoint.
   */
  public rollback() {
    const setContent = async (
      promise: Promise<string | null | undefined> | undefined,
    ) => {
      const content = await promise;
      if (content != null) this._gui?.setContent(content);
    };
    const mode = this.capytale.capytaleMode();
    if (mode === "student") {
      this._gui?.confirm(
        "Revenir à une version précédente",
        "Que voulez-vous faire ?",
        "Restaurer une sauvegarde",
        () => setContent(this._gui?.selectCheckpoint()),
        "Revenir à la version de l'enseignant",
        () => {
          this._gui?.confirm(
            "Revenir à la version de l'enseignant",
            "Voulez-vous vraiment revenir à la version de l'enseignant et perdre vos modifications ?",
            "Confirmer",
            () => setContent(this.capytale.activityContent()),
            "Annuler",
            () => {},
          );
        },
      );
    } else {
      setContent(this._gui?.selectCheckpoint());
    }
  }

  /**
   * Update UI and entity to new assignment state.
   */
  public async setAssignmentState(state: string) {
    const mode = this.capytale.capytaleMode();
    // set new state
    this.capytale.set("workflow", ASSIGNMENT_STATE[state.toUpperCase()]);
    // save
    await this.save(["workflow"]);

    // displaying current state
    this.hideAllAssignmentInfo();
    const info = document.getElementById(
      `capytale-assignment-state-${mode}-${state.toLowerCase()}`,
    ) as HTMLElement;
    if (info != null) info.style.display = "";

    // disable/enable save button
    if (this.capytale.isReadOnly()) this.disableSaveButton();
    else this.enableSaveButton();
  }

  /**
   * Display an error message and return true if it's dirty.
   * return false otherwise.
   */
  public saySaveBeforeChangeAssignmentState() {
    if (this.dirty()) {
      this._gui?.error(
        "Veuillez enregistrer avant !",
        "Il est nécessaire d'enregistrer la copie avant d'en changer le statut.",
      );
      return true;
    }
    return false;
  }

  /**
   * Hide all assignment sate info in UI.
   */
  public hideAllAssignmentInfo() {
    const mode = this.capytale.capytaleMode();
    for (let state of ["ongoing", "returned", "corrected"]) {
      const info = document.getElementById(
        `capytale-assignment-state-${mode}-${state}`,
      );
      if (info != null) info.style.display = "none";
    }
  }

  /**
   * List files imported/created during the session by the user
   * (in the homedir).
   */
  public async sessionFiles(): Promise<string[]> {
    return await this.capytale.sessionFiles();
  }

  /**
   * List modules imported during the session by the user from the UI.
   */
  public async sessionModules(): Promise<string[]> {
    // remove _*.pyc files
    const modules = (
      (await this._gui?.kernelSafe?.userModules?.()) ?? []
    )?.filter((x: string) => !(x.startsWith("_") && x.endsWith(".pyc")));
    const attached = new Set(this.capytale.attachedFilesByName());
    return modules?.filter((x: string) => !attached.has(x));
  }

  /**
   * List files and modules imported/created during the session
   * by the user. This is the concatenation of sessionFiles and
   * sessionModules.
   */
  public async sessionAux(): Promise<string[]> {
    return (await this.sessionFiles()).concat(await this.sessionModules());
  }
}
