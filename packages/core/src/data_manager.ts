import { ASSIGNMENT_STATE, Capytale, Entity } from "./capytale";
import { textEditors } from "@basthon-capytale/text-editors";

declare global {
  interface Window {
    _capytaleEditor?: any;
  }
}

type Manager = { [key: string]: any };

/**
 * An object to manager entity and page content.
 *
 * frontend should be 'console' or 'notebook'.
 */
export class DataManager {
  private _capytale: Capytale;
  private _contentName: string;
  protected _managers: { [key: string]: Manager };

  public constructor(capytale: Capytale) {
    this._capytale = capytale;
    /**
     * Main variables to handle factoring.
     */
    const frontend: string = this._capytale.frontend;
    this._contentName =
      {
        notebook: "notebook",
        console: "script",
      }[frontend] || "";
    /**
     * The object containing data to manage (aka. managers).
     */
    this._managers = {
      me: {
        entity: "me",
        field: "",
      },
      student: {
        entity: "student_assignment",
        field: "uid",
        getter: "target_id",
      },
      creator: {
        entity: `${frontend}_activity`,
        field: "uid",
        getter: "target_id",
      },
      token: {
        entity: "token",
        field: "",
      },
      "teacher-activity": {
        entity: "student_assignment",
        field: "field_activity",
        getter: "target_id",
      },
      attached_files: {
        entity: `${frontend}_activity`,
        "preserve-arrays": true,
        getter: "url",
      },
      associates: {
        entity: `${frontend}_activity`,
        "preserve-arrays": true,
        getter: "target_id",
      },
      activity_type: {
        entity: `${frontend}_activity`,
        "edit-modes": ["teacher-edit", "student-edit"],
      },
      title: {
        entity: `${frontend}_activity`,
        header: true,
        type: "text",
        field: "title",
        "edit-modes": ["teacher-edit", "student-edit"],
        "visible-modes": "all",
      },
      "title-student": {
        entity: "student_assignment",
        type: "text",
        field: "title",
        "edit-modes": ["student"],
        // this will force replacing student title by teacher activity one (see capytale.save)
        "dom-id": "capytale-title",
      },
      description: {
        entity: `${frontend}_activity`,
        header: true,
        type: "text",
        "edit-modes": ["teacher-edit", "student-edit"],
        editor: "rich",
        "visible-modes": "all",
      },
      appreciation: {
        entity: "student_assignment",
        header: true,
        type: "text",
        "edit-modes": ["teacher-review"],
        "visible-modes": ["teacher-review", "student"],
      },
      evaluation: {
        entity: "student_assignment",
        header: true,
        type: "text",
        "edit-modes": ["teacher-review"],
        "visible-modes": ["teacher-review", "student"],
      },
      workflow: {
        entity: "student_assignment",
        "edit-modes": ["student", "teacher-review"],
      },
      "assignment-state-student-ongoing": {
        header: true,
        "visible-modes": ["student"],
        "visible-states": [ASSIGNMENT_STATE.ONGOING],
        keep: true,
      },
      "assignment-state-student-returned": {
        header: true,
        "visible-modes": ["student"],
        "visible-states": [ASSIGNMENT_STATE.RETURNED],
        keep: true,
      },
      "assignment-state-student-corrected": {
        header: true,
        "visible-modes": ["student"],
        "visible-states": [ASSIGNMENT_STATE.CORRECTED],
        keep: true,
      },
      "assignment-state-teacher-review-ongoing": {
        header: true,
        "visible-modes": ["teacher-review"],
        "visible-states": [ASSIGNMENT_STATE.ONGOING],
        keep: true,
      },
      "assignment-state-teacher-review-returned": {
        header: true,
        "visible-modes": ["teacher-review"],
        "visible-states": [ASSIGNMENT_STATE.RETURNED],
        keep: true,
      },
      "assignment-state-teacher-review-corrected": {
        header: true,
        "visible-modes": ["teacher-review"],
        "visible-states": [ASSIGNMENT_STATE.CORRECTED],
        keep: true,
      },
      clonable: {
        entity: `${frontend}_activity`,
        header: true,
        field: "field_status_clonable",
        type: "checkbox",
        "edit-modes": ["teacher-edit"],
        "visible-modes": ["teacher-edit"],
      },
      [`activity-${this.contentName}`]: {
        entity: "activity-content",
        field: "field_content",
        type: "text",
        "edit-modes": ["teacher-edit", "student-edit"],
        "dom-getter": () => window._capytaleEditor,
        "visible-modes": ["teacher-edit", "student-edit", "share"],
      },
      [`student-${this.contentName}`]: {
        entity: "student-content",
        field: "field_content",
        type: "text",
        "edit-modes": ["teacher-review", "student"],
        "dom-getter": () => window._capytaleEditor,
        "visible-modes": ["teacher-review", "student"],
      },
      "student-name": {
        entity: "userinfo",
        field: "field_nom",
      },
      "student-firstname": {
        entity: "userinfo",
        field: "field_prenom",
      },
      "student-class": {
        entity: "userinfo",
        field: "field_classe",
      },
      "student-info": {
        entity: "multiple",
        header: true,
        type: "text",
        "visible-modes": ["teacher-review"],
        getter: (e: Entity) =>
          e["student-firstname"] +
          " " +
          e["student-name"] +
          " — " +
          e["student-class"],
      },
      "student-code": {
        entity: `${frontend}_activity`,
        header: true,
        type: "text",
        field: "field_code",
        "visible-modes": ["teacher-edit"],
      },
      "time-range-mode": {
        entity: `${frontend}_activity`,
        field: "field_access_tr_mode",
      },
      "time-range-start": {
        entity: `${frontend}_activity`,
        field: "field_access_timerange",
      },
      "time-range-end": {
        entity: `${frontend}_activity`,
        field: "field_access_timerange",
        getter: "end_value",
      },
      "share-with-teacher-container": {
        header: true,
        "visible-modes": ["student-edit"],
      },
      "file-list": {
        entity: "multiple",
        type: "text",
        "safe-html": true, // force sanitizer bypass otherwise it drops class attributes
        getter: (e: Entity) => {
          e = (e["attached_files"] || [])
            .reduce((filtered: string[], x: string) => {
              const filename = capytale.attachedFileName(x) as string;
              const hidden = capytale.isHiddenFile(filename);
              const show =
                !hidden ||
                ["teacher-edit", "teacher-review", "share"].includes(
                  capytale.capytaleMode()?.toString() ?? ""
                );
              let item = filename;
              if (hidden && show) {
                const title =
                  "Ce module sera importable par les élèves mais ne sera pas visible dans ce menu";
                item = `${item}&nbsp;&nbsp;<i class="fa fa-eye-slash" title="${title}"></i>`;
              }
              if (show) filtered.push(item);
              return filtered;
            }, [])
            .join("</div></div><div><div>");
          return e ? "<div><div>" + e + "</div></div>" : "";
        },
      },
      share: {
        header: true,
        "visible-modes": [],
        "dom-id": "share",
      },
    };
  }

  /**
   * Content name getter.
   */
  public get contentName() {
    return this._contentName;
  }

  /**
   * Initialize the data manager.
   *
   * Basically it insert default values to internal object.
   */
  public init() {
    /* inserting default values */
    for (let d in this._managers) {
      const manager = this._managers[d];
      if (!("entity" in manager)) manager["entity"] = false;
      if (manager["entity"] && !("field" in manager))
        manager["field"] = "field_" + d;
      if (manager["entity"] && !("getter" in manager))
        manager["getter"] = "value";
      if (!("dom-id" in manager || "dom-getter" in manager))
        manager["dom-id"] = "capytale-" + d;
    }
  }

  /**
   * Get all managers.
   */
  public managers() {
    return this._managers;
  }

  /**
   * Get specific manager.
   */
  public manager(data: string) {
    return this._managers[data];
  }

  /**
   * Is data editable?
   */
  public editable(data: string) {
    const manager = this.manager(data);
    const mode = this._capytale.capytaleMode();
    const modes = manager["edit-modes"];
    return modes && modes.includes(mode);
  }

  /**
   * Force safe html to bypass sanitizer.
   */
  public safeHTML(data: string) {
    const manager = this.manager(data);
    return manager["safe-html"] ?? false;
  }

  /**
   * Get an array of all editable managers.
   */
  public editables() {
    const res = [];
    for (let d in this._managers) if (this.editable(d)) res.push(d);
    return res;
  }

  /**
   * Get editor mode from manager.
   */
  public editor(data: string) {
    const manager = this.manager(data);
    return manager["editor"];
  }

  /**
   * Is data visible?
   */
  public visible(data: string): boolean {
    const manager = this.manager(data);
    const visibleModes = manager["visible-modes"];
    const mode = this._capytale.capytaleMode();
    let visibleStates = manager["visible-states"];
    if (visibleStates == null) visibleStates = "all";
    const state = this._capytale.assignmentState();
    return (
      (visibleModes === "all" || visibleModes.includes(mode)) &&
      (visibleStates === "all" || visibleStates.includes(state))
    );
  }

  /**
   * Does the element should be kept when not visible?
   * Otherwise it will be removed.
   */
  public keepIfHidden(data: string) {
    const manager = this.manager(data);
    return manager["keep"];
  }

  /**
   * Is data displayed in header?
   */
  public inHeader(data: string) {
    const manager = this.manager(data);
    return manager["header"];
  }

  /**
   * Return an array of all data from header.
   */
  public headerDatas() {
    const res = [];
    for (let d in this._managers) if (this.inHeader(d)) res.push(d);
    return res;
  }

  /**
   * Get data type.
   */
  public type(data: string) {
    const manager = this.manager(data);
    return manager["type"];
  }

  /**
   * Is this textual data?
   */
  public isText(data: string) {
    return this.type(data) === "text";
  }

  /**
   * Is this checkbox data?
   */
  public isCheckbox(data: string) {
    return this.type(data) === "checkbox";
  }

  /**
   * Return the document id of this data.
   */
  public domId(data: string) {
    const manager = this.manager(data);
    return manager["dom-id"];
  }

  /**
   * Return the DOM element of this data.
   */
  public domElement(data: string): HTMLElement | null {
    const manager = this.manager(data);
    const getter = manager["dom-getter"];
    if (getter != null) return getter() as unknown as HTMLElement | null;
    else return document.getElementById(this.domId(data));
  }

  /**
   * Return the content of the data (from document).
   */
  public content(data: string): string | undefined {
    const elem = this.domElement(data);
    if (this.isText(data)) {
      if (this.inHeader(data)) {
        const domId = this.domId(data);
        const editor = textEditors.get(domId);
        if (editor) return editor.content();
        else if (elem?.tagName === "TEXTAREA")
          return (elem as HTMLTextAreaElement).value;
        else return elem?.innerText;
      } else if ((elem as any)?.getValue) {
        // text area not in header, this shoud be script!
        return (elem as any).getValue();
      } else return elem?.innerText;
    } else if (this.isCheckbox(data)) {
      return (elem as any)?.checked;
    } else {
      return this._capytale.get(data);
    }
  }

  /**
   * Get all editable content.
   */
  public editableContents() {
    const data: { [key: string]: string | undefined } = {};
    for (const d of this.editables()) {
      data[d] = this.content(d);
    }
    return data;
  }

  /**
   * Get all editable content from Capytale entity.
   */
  public editableContentsFromEntity() {
    const data: { [key: string]: string | undefined } = {};
    for (const d of this.editables()) {
      data[d] = this._capytale.get(d);
    }
    return data;
  }
}
