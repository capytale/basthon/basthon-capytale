import { PromiseDelegate } from "promise-delegate";
import { KernelBase } from "@basthon/kernel-base";
import { Python3Kernel } from "@basthon/kernel-python3";
import { DataManager } from "./data_manager";
export { DataManager } from "./data_manager";

declare global {
  interface Window {
    // for matomo
    _paq?: (string | number)[][];
  }
}

/**
 * Custom exception for Capytale errors handling.
 */
export class CapytaleException extends Error {
  public data: any;
  constructor(message: string, data?: any) {
    super(message);
    this.name = "CapytaleException";
    this.data = data;
  }
}

export type Entity = { [key: string]: any };
export type CapytaleMode =
  | "teacher-edit"
  | "teacher-review"
  | "student"
  | "share"
  | "student-edit";
export type AssignmentState = 100 | 200 | 300;

/**
 * Assignment states
 */
export const ASSIGNMENT_STATE: { [key: string]: AssignmentState } = {
  ONGOING: 100,
  RETURNED: 200,
  READONLY: 200,
  CORRECTED: 300,
};

/**
 * A class for interacting with Capytale.
 *
 * frontend should be 'console' or 'notebook'.
 */
export class Capytale {
  /**
   * Two main attributes to handle factoring.
   */
  private _frontend: string;
  /**
   * Python is started in this specific folder.
   * This is is cleaner and is usefull to list user files
   * (code created or imported through the UI).
   */
  private readonly homeDir = "/home/capytale_user";
  private _kernelReady = new PromiseDelegate<void>();
  private readonly _reHiddenFiles = /^_.*\.py$/;
  private _forcedMode: CapytaleMode = (() => {
    // get mode=... URL parameter
    // this parameter is used to force a mode when a teacher
    // opens his own student assignment
    const url = new URL(window.location.href);
    if (url.searchParams.get("mode") === "review") return "teacher-review";
    return "student";
  })();

  /**
   * URL base for all entities.
   */
  private _baseURL: string = (() => {
    const url = new URL(window.location.href);
    // is it p/basthon or just basthon?
    const hasP = url.pathname.includes("p/basthon");
    const splitedPath = url.pathname.split("/");
    // relevant par is just before basthon or p/basthon
    const path = splitedPath.slice(
      0,
      splitedPath.indexOf("basthon") - Number(hasP),
    );
    path.push("web", ""); // ensure leading '/'
    url.pathname = path.join("/");
    // no # or ? part
    url.hash = "";
    url.search = "";
    return url.toString();
  })();

  /**
   * Several usefull base URL for Capytale requests.
   */
  private _baseURLs: { [key: string]: string } = (() => {
    const base = this._baseURL;
    return {
      /* base URL for current (connected) user request. */
      root: base,
      // URL to get logged user info
      me: `${base}c-auth/api/me`,
      // base URL to read ${frontend}_activity and student_assignement.
      activities: `${base}node/`,
      // base URL to read content from the file API.
      "file-content": `${base}c-act/api/n/{nid}/fields/content`,
      // base URL to get token.
      token: `${base}session/token`,
      // base URL to get user info
      userinfo: `${base}userinfos?uid_raw=`,
      // base URL to go back to activities page in Capytale
      my: `${base}my`,
      assignments: `${base}assignments/`,
      // base URL to go back to library page in Capytale
      bib: `${base}bibliotheque/`,
      // url for server time
      time: `${base}../vanilla/time-s.php`,
      // url for site id
      siteid: `${base}../vanilla/site-id.php`,
    };
  })();

  /**
   * Entity build for Basthon from several Capytale entities
   */
  private _entity: Entity | undefined;

  /**
   * Tracking the loading state and the mode
   * (teacher-edit, teacher-review, student, share...).
   */
  private _capytaleMode?: CapytaleMode | null;

  /**
   * Tracking the assignment state in student/teacher-review mode
   * (ongoing, returned, corrected).
   */
  private _assignmentState?: AssignmentState;
  //"ongoing" | "returned" | "corrected" | undefined;

  /**
   * The entity we want to access.
   */
  private _entityId: string | null;

  /**
   * The LTI object containing LTI stuff.
   */
  private _lti:
    | {
        token: string;
        props: {
          returnUrl: string;
          formUrl: string;
        };
        headers: { [key: string]: string };
      }
    | undefined;

  /**
   * The URL parameter key to request entity (private).
   */
  private _paramsKey = "id";

  /**
   * The running Basthon's kernel.
   */
  private _kernel?: KernelBase;

  /**
   * Entity data manager.
   */
  private _dataManager: DataManager;

  /**
   * Timestamp from server.
   */
  private _serverRefTimestamp: number = 0;

  /**
   * Timestamp when server time is taken.
   */
  private _localRefTimestamp: number = 0;

  public constructor(frontend: string, DataManagerClass: typeof DataManager) {
    this._frontend = frontend;

    /**
     * Backuping entity id.
     */
    const url = new URL(window.location.href);
    this._entityId = url.searchParams.get(this._paramsKey);

    // get lti stuff
    const ltiSessionId = url.searchParams.get("lti");
    if (ltiSessionId != null) {
      const token = sessionStorage.getItem(`lti_session_${ltiSessionId}`) ?? "";
      this._lti = {
        token,
        props: JSON.parse(
          sessionStorage.getItem(`lti_props_${ltiSessionId}`) ?? "",
        ),
        headers: { "X-CAPYTALE-AUTH": `lti ${token}` },
      };
    }

    this._dataManager = new DataManagerClass(this);
  }

  /**
   * DataManager getter.
   */
  public get dataManager() {
    return this._dataManager;
  }

  /**
   * Entity Id getter.
   */
  public get entityId() {
    return this._entityId;
  }

  /**
   * Set the Basthon's kernel.
   */
  public setKernel(kernel: KernelBase) {
    this._kernel = kernel;
    // go into specific homedir when using Python3
    (async () => {
      if (kernel.language() === "python3") {
        await kernel.ready();
        const _kernel = kernel as Python3Kernel;
        await _kernel.mkdir(this.homeDir);
        await _kernel.chdir(this.homeDir);
      }
      this._kernelReady.resolve();
    })();
  }

  /**
   * Promise to wait for Capytale's modifications to kernel.
   */
  public async kernelReady(): Promise<void> {
    return await this._kernelReady.promise;
  }

  /**
   * Base URLs mainly for internal Capytale requests.
   */
  public url(type: string) {
    type = type || "root";
    return this._baseURLs[type];
  }

  /**
   * URL to entity edition page.
   */
  public editURL() {
    if (this._lti == null)
      return `${this.url("activities")}${this.entityId}/edit?kernel=${
        this._kernel?.language() ?? ""
      }`;
    else return this._lti?.props?.formUrl;
  }

  /**
   * Basthon entity getter.
   */
  public get entity() {
    return this._entity;
  }

  /**
   * Get frontend string.
   */
  public get frontend() {
    return this._frontend;
  }

  /**
   * Basthon entity field getter.
   */
  public get(data: string) {
    return this._entity?.[data];
  }

  /**
   * Basthon entity field setter.
   */
  public set(data: string, value: any) {
    if (this._entity != null) this._entity[data] = value;
  }

  /**
   * Return the Capytale mode:
   *   - if id is *not* in URL:
   *     - false
   *   - if id *is* in URL:
   *     - true if entity is not yet requested
   *     - null if entity request failed
   *     - if request succeded:
   *       - "teacher-edit"
   *       - "student-edit"
   *       - "student"
   *       - "teacher-review"
   *       - "share"
   */
  public capytaleMode() {
    if (typeof this._capytaleMode === "undefined") {
      const url = new URL(window.location.href);
      return url.searchParams.has(this._paramsKey);
    }
    return this._capytaleMode;
  }

  /**
   * The URL behind the back button.
   */
  public returnURL(): string {
    if (this._lti == null) {
      switch (this.capytaleMode()) {
        case "teacher-review":
          return this.url("assignments") + this.get("teacher-activity");
        case "share":
          return this.url("bib");
        default:
          return this.url("my");
      }
    } else {
      return this._lti.props.returnUrl;
    }
  }

  /**
   * Return the Capytale assignment state
   * (undefined if it makes no sens).
   */
  public assignmentState() {
    return this._assignmentState;
  }

  /**
   * Return the Capytale assignment state as text.
   */
  public assignmentStateText(): string {
    const state = this._assignmentState;
    if (state != null)
      for (const s in ASSIGNMENT_STATE)
        if (ASSIGNMENT_STATE[s] === state) return s;
    return "undefined";
  }

  /**
   * Build URL for a request to Capytale REST API.
   */
  private _requestURL(url: string, ressource?: string | null) {
    const _url = new URL(url);
    if (ressource) _url.pathname += ressource;
    _url.searchParams.set("_format", "json");
    return _url;
  }

  /**
   * Raw GET request to Capytale REST API.
   * Used for ${frontend}_activity, student_assignment
   * and user requests (common part).
   */
  private async _loadRaw(
    url: string,
    ressource: string | null,
    errorMessage: string,
    responseType: string = "json",
  ) {
    const _url = this._requestURL(url, ressource);
    let json: any;
    try {
      const response = await fetch(_url.toString(), {
        method: "GET",
        headers: { "Content-Type": "application/json", ...this._lti?.headers },
      });
      if (response.ok) {
        if (responseType === "text") json = await response.text();
        else json = await response.json();
      } else throw new Error(`Network error (status: ${response.status})`);
    } catch (error) {
      throw new CapytaleException(errorMessage);
    }

    json = this.getRidOfUselessArray(json);
    if (json == null)
      throw new CapytaleException(
        "L'entité est vide. " +
          "Vérifiez que vous êtes bien connecté à Capytale !",
      );
    return json;
  }

  private async _matomoInit(muuid: string) {
    // wait for kernel to be ready
    await this.kernelReady();
    // get siteId
    const siteId = await (async () => {
      const response = await fetch(this.url("siteid"));
      if (response.ok) return response.text();
    })();
    if (siteId == null) throw new Error("Internal error initializing matomo.");
    let _paq = (window._paq = window._paq ?? []);
    _paq.push(["setUserId", muuid]);
    // tracker methods like "setCustomDimension" should be called *with* "trackPageView"
    // see https://developer.matomo.org/guides/tracking-javascript-guide#tracking-a-custom-dimension-for-one-specific-action-only
    const dims: any = {
      // we track the Basthon's context
      dimension4:
        `{coi: ${window.crossOriginIsolated === true}, ` +
        //@ts-ignore
        `legacy: ${this._kernel?.isLegacy == null || this._kernel?.isLegacy?.() === true}}`,
    };
    // we track the CDN URL used to load Pyodide
    if (this._kernel!.language() === "python3") {
      //@ts-ignore
      const pyodideURL: string = pyodide?._api?.config?.indexURL;
      if (pyodideURL != null) dims.dimension3 = pyodideURL;
    }
    _paq.push(["trackPageView", undefined, dims]);
    _paq.push(["enableLinkTracking"]);
    (function () {
      var u = "https://matomo.ac-paris.fr/";
      _paq.push(["setTrackerUrl", u + "matomo.php"]);
      _paq.push(["setSiteId", siteId]);
      var d = document,
        g = d.createElement("script"),
        s = d.getElementsByTagName("script")[0];
      g.async = true;
      g.src = u + "matomo.js";
      s?.parentNode?.insertBefore(g, s);
    })();
  }

  /**
   * Loading user entity (to get uid for connected user)
   */
  private async _loadRawUserEntity() {
    const message =
      "Impossible de récupérer les informations de " +
      "l'utilisateur. Vous n'êtes pas connecté à Capytale !";
    const me = await this._loadRaw(this.url("me"), null, message);
    // postpone matomo init
    (async () => {
      try {
        await this._matomoInit(me["muuid"]);
      } catch (e) {
        console.error("Can't set muuid!");
      }
    })();
    try {
      return { value: me["uid"] };
    } catch (e) {
      throw new CapytaleException(message);
    }
  }

  /**
   * Loading raw entity for later process by loadEntity.
   */
  private async _loadRawEntity(id: string) {
    return await this._loadRaw(
      this.url("activities"),
      id,
      `Impossible de récupérer l'entité ${id}.`,
    );
  }

  /**
   * Get a token in order to make CSRF request.
   */
  private async _loadRawToken() {
    // returning encapsulated token to fit data _loadRawEntities
    return {
      value: await this._loadRaw(
        this.url("token"),
        null,
        "Impossible de récupérer le token pour la sauvegarde.",
        "text",
      ),
    };
  }

  /**
   * Length one arrays are filtered to get there single element.
   */
  public getRidOfUselessArray(obj: any[] | null) {
    if (obj != null)
      switch (obj.length) {
        case 0:
          return;
        case 1:
          return obj[0];
      }
    return obj;
  }

  /**
   * Return the activity (teacher) content (network request).
   */
  public async activityContent(): Promise<string | null> {
    if (this.frontend === "notebook")
      return await this._contentFromFileAPI(this.get("teacher-activity"));
    else return this.get("activity-content");
  }

  /**
   * Call the file API to get the content from nid.
   */
  private async _contentFromFileAPI(nid: number): Promise<string | null> {
    const response = await fetch(
      this.url("file-content").replace("{nid}", nid.toString()),
      {
        method: "GET",
        headers: {
          "X-API-ACCESS": "capytale",
          ...this._lti?.headers,
        },
      },
    );
    if (response.ok) {
      switch (response.status) {
        case 204:
          return null;
        default:
          return await response.text();
      }
    } else {
      let message = `Impossible d'accèder au contenu de l'entité ${nid}.`;
      switch (response.status) {
        case 403:
          message = `Vous n'avez pas l'autorisation d'accèder au contenu de l'entité ${nid}.`;
          break;
        case 404:
          message = `Le contenu de l'entité ${nid} n'existe pas.`;
          break;
      }
      throw new CapytaleException(message);
    }
  }

  /**
   * Load content knowing the entity (choose to get content from entity
   * or from the file API).
   */
  private async _contentFromEntity(entity: Entity): Promise<Entity> {
    if (this.frontend === "notebook")
      return {
        field_content: [
          {
            value: await this._contentFromFileAPI(entity["nid"][0].value),
          },
        ],
      };
    else return entity;
  }

  /**
   * Loading all entities, raw (not processed),
   * directly from Capytale.
   */
  private async _loadRawEntities() {
    const entities: Entity = {};
    entities["me"] = await this._loadRawUserEntity();
    if (this._lti == null) entities["token"] = await this._loadRawToken();

    // guess main entity is an activity, correct later
    if (this.entityId == null) return;
    let activity = await this._loadRawEntity(this.entityId);
    if ("field_activity" in activity) {
      // oups! this is a student assignment!
      entities["student_assignment"] = activity;
      entities["student-content"] = await this._contentFromEntity(activity);
      const id = activity["field_activity"][0].target_id;
      activity = await this._loadRawEntity(id);
    }
    // we get activity content only when necessary (no student or student content null)
    if (
      this.frontend !== "notebook" ||
      entities?.["student-content"]?.["field_content"]?.[0]?.value == null
    ) {
      entities["activity-content"] = await this._contentFromEntity(activity);
    }
    entities[`${this._frontend}_activity`] = activity;

    // userinfo
    if ("student_assignment" in entities) {
      // teacher-review or student mode
      const me = entities["me"]["value"];
      const uid = entities["student_assignment"]["uid"][0].target_id;
      // teacher-review mode?
      if (this._forcedMode === "teacher-review" || me !== uid)
        entities["userinfo"] = await this._loadRaw(
          this.url("userinfo") + uid,
          null,
          "Impossible de récupérer l'identité de l'utilisateur " + uid + ".",
        );
    }

    // just to force lookup for combined entities
    entities["multiple"] = true;

    return entities;
  }

  /**
   * Filtering entity to fit Basthon API (using data manager).
   */
  private _populateEntity(entities: Entity, name: string, entity: Entity) {
    if (!(name in entities)) return;

    const rawEntity = entities[name];
    for (let data in this._dataManager.managers()) {
      const manager = this._dataManager.manager(data);
      if (manager["entity"] !== name) continue;
      // case of combined content
      if (manager["entity"] === "multiple") {
        entity[data] = manager["getter"](entity);
        continue;
      }
      // default case
      let value;
      let field = manager["field"];
      if (field === "") {
        value = rawEntity; // typically for 'token' and 'me'
      } else if (!(field in rawEntity && rawEntity[field].length)) {
        continue;
      } else {
        value = rawEntity[field];
      }
      const getter = manager["getter"];
      if (manager["preserve-arrays"])
        entity[data] = value.map((x: any) => x[getter]);
      else entity[data] = this.getRidOfUselessArray(value)[getter];
    }
  }

  /**
   * Load all usefull fields from multiple entities to build our own
   * combined entity for Basthon.
   */
  public async loadEntity() {
    this._capytaleMode = null; // set later in case of success (see below)
    const entities = await this._loadRawEntities();
    this._entity = {};

    // early capytale mode resolution
    // datas of type 'multiple' are computed after capytaleMode
    // resolution so tha they can access it
    // (see tipycally file-list-menu in notebook)
    for (const name in entities)
      if (name !== "multiple")
        this._populateEntity(entities, name, this._entity);

    this._capytaleMode = this._resolveCapytaleMode();

    for (const name in entities)
      if (name === "multiple")
        this._populateEntity(entities, name, this._entity);

    this._assignmentState = this._entity["workflow"];

    // build server time if needed
    if (this.timeRangeEnabled()) await this._initServerTime();
  }

  /**
   * Initialize the server time.
   */
  private async _initServerTime() {
    // trying 3 times to get server time
    let response,
      t0 = 0,
      t1 = 0;
    for (let i = 0; i < 3; i++) {
      t0 = performance.now();
      response = await fetch(this.url("time"));
      t1 = performance.now();
      if (response.ok && t1 - t0 < 2000) break;
    }
    // build referent timestamps
    let timestamp = Date.now(); // fall back to local time
    if (response?.ok) {
      try {
        timestamp = parseInt(await response?.text()) * 1000;
      } catch (e) {}
    }
    this._serverRefTimestamp = timestamp;
    this._localRefTimestamp = (t0 + t1) / 2;
  }

  /**
   * Retreive the Capytale mode from entity
   * (precondition: request succeeded).
   */
  private _resolveCapytaleMode(): CapytaleMode | null {
    const entity: Entity = this.entity || {};
    const associates = entity["associates"] || [];

    const me = entity["me"],
      creator = entity["creator"],
      student = entity["student"];
    const activityManager = me === creator || associates.includes(me);
    const studentCode = entity["student-code"] || "";

    if (me == null || creator == null) return null;

    if (student != null) {
      // ambigous case: teacher read his own student copy
      if (activityManager && me === student) return this._forcedMode;
      // activity is of type 'student_assignment'
      if (me === student) return "student";
      if (activityManager) return "teacher-review";
      throw new CapytaleException(
        "Erreur interne : mode de consultation non implémenté.",
      );
    } else {
      // activity is of type '..._activity'
      if (activityManager) {
        // student or teacher?
        if (studentCode === "student_activity") return "student-edit";
        return "teacher-edit";
      }
      return "share";
    }
  }

  /**
   * Is time-range enabled?
   */
  public timeRangeEnabled() {
    const mode = this.get("time-range-mode");
    return ["complete", "lock"].includes(mode);
  }

  /**
   * Get the time range as an object with start and end fields.
   */
  public timeRange() {
    let start: Date | string = this.get("time-range-start");
    if (typeof start === "string" || start instanceof String) {
      start = new Date(start);
      this.set("time-range-start", start);
    }
    let end: Date | string = new Date(this.get("time-range-end"));
    if (typeof end === "string" || end instanceof String) {
      end = new Date(end);
      this.set("time-range-end", end);
    }
    return { start: start, end: end };
  }

  /**
   * Same as Date.now but using the server time, not the local one.
   */
  public now() {
    return (
      this._serverRefTimestamp + (performance.now() - this._localRefTimestamp)
    );
  }

  /**
   * Check if the activity can be modified.
   */
  public isReadOnly() {
    const mode = this.capytaleMode();
    if (mode === "share") return true;
    if (mode === "student") {
      if (
        (this.assignmentState() as AssignmentState) >= ASSIGNMENT_STATE.READONLY
      )
        return true;
      if (this.timeRangeEnabled()) {
        const { start, end } = this.timeRange();
        const now = new Date(this.now());
        return !(start <= now && now < end);
      }
    }
    return false;
  }

  /**
   * Get filename from URL for an attached file.
   * Bypassing the Drupal renaming process
   * \.py(_[0-9]+)?\.txt$ -> .py
   */
  public attachedFileName(url: string) {
    const filename = url.split("/").pop();
    return filename?.replace(/\.py_(\d+)?\.txt$/, ".py");
  }

  /**
   * Get the list of attached files by name (not full URL).
   */
  public attachedFilesByName() {
    const attachedFiles = this.get("attached_files") || [];
    return attachedFiles.map(this.attachedFileName.bind(this));
  }

  /**
   * Loading attached standard files and preparing
   * Python modules for import.
   */
  public async loadFilesFromEntity() {
    await this.kernelReady();
    const attachedFiles = this.get("attached_files");
    if (!attachedFiles) return;
    // loading all ressources concurently.
    let promises = [];
    for (let fileURL of attachedFiles) {
      const filename = this.attachedFileName(fileURL);
      if (filename == null) continue;
      const promise = fetch(fileURL, { headers: this._lti?.headers })
        .then((response: Response) => {
          if (response.ok) return response.arrayBuffer();
          else throw new Error(`Network error (status: ${response.status}).`);
        })
        .then((data: ArrayBuffer) => {
          return this._kernel?.putRessource(filename, data);
        })
        .catch(function (error: Error | PromiseRejectionEvent) {
          let message: string = (error as Error).message;
          if (message == null)
            message = (error as PromiseRejectionEvent).reason?.message;
          if (message == null) message = error.toString();
          throw new CapytaleException(
            `Impossible de charger le fichier ${filename}.\n${message}`,
          );
        });
      promises.push(promise);
    }
    await Promise.all(promises);
  }

  /**
   * List session files.
   */
  public async sessionFiles(): Promise<string[]> {
    await this.kernelReady();
    if (this._kernel!.language() !== "python3") return [];
    const kernel = this._kernel as Python3Kernel;
    const inHome = (await kernel.readdir(this.homeDir)).filter(
      (f: string) => f !== "." && f !== ".." && !this.isHiddenFile(f),
    );
    const attached = new Set(this.attachedFilesByName());
    return inHome.filter((x: string) => !attached.has(x));
  }

  /**
   * Return true if filename is hidden (should not be displayed in GUI).
   */
  public isHiddenFile(filename: string) {
    return this._reHiddenFiles.test(filename);
  }

  /**
   * Save the script/notebook via CSRF token based request.
   */
  public async saveContent(src: string) {
    // one of those will be filtered out by this.save
    const contentName = this._dataManager.contentName;
    return this.save({
      [`activity-${contentName}`]: src,
      [`student-${contentName}`]: src,
    });
  }

  /**
   * Save data to Capytale via CSRF token based request.
   */
  public async save(data: { [key: string]: any }) {
    // building backup data
    let backup: Entity = {};
    const mode = this.capytaleMode();

    // no backup in share mode
    if (mode === "share")
      throw new CapytaleException(
        "Impossible de sauvegarder une entité partagée.",
      );

    // in student mode, check that we respect read-only flag
    // here we are sure to not be in share mode
    if (this.isReadOnly())
      throw new CapytaleException(
        "Impossible de sauvegarder une activité déjà rendue.",
      );

    // assignment state checks
    if ("workflow" in data) {
      // check for mode (this should not happend)
      if (!(mode === "teacher-review" || mode === "student"))
        throw new CapytaleException(
          "Le statut de l'activité ne peut pas être modifié dans ce mode de consultation.",
        );
      // check for supported assignment state
      if (!Object.values(ASSIGNMENT_STATE).includes(data["workflow"]))
        throw new CapytaleException("Statut de l'activité non supporté.");
      // in student mode, restricted set for workflow
      if (
        mode === "student" &&
        ![ASSIGNMENT_STATE.ONGOING, ASSIGNMENT_STATE.RETURNED].includes(
          data["workflow"],
        )
      )
        throw new CapytaleException(
          "Impossible de modifier le statut de l'activité.",
        );
    }

    for (let d in data) {
      const content = data[d];
      const manager = this._dataManager.manager(d);
      const field = manager["field"];
      if (this._dataManager.editable(d))
        backup[field] = [{ [manager["getter"]]: content }];
    }

    // exit if backup is empty
    if (Object.keys(backup).length === 0 && backup.constructor === Object)
      return;

    let fileContent: string | null = null;
    // correction for content using the file API
    const field = "field_content";
    if (this.frontend === "notebook" && backup[field] != null) {
      fileContent = backup[field][0].value;
      delete backup[field];
    }

    if (fileContent != null)
      await this._sendRequest(() =>
        this._saveContentFile(fileContent as string),
      );

    // setting entity type, data is now ready.
    let target = "student_assignment";
    if (mode === "teacher-edit" || mode === "student-edit")
      target = `${this._frontend}_activity`;
    backup["type"] = [{ target_id: target }];

    await this._sendRequest(() => this._saveEntity(backup));

    // assignment state has changed? update it.
    if ("workflow" in data) this._assignmentState = data["workflow"];
  }

  /**
   * Save the entity (network request)
   */
  private async _saveEntity(entity: Entity) {
    const response = await fetch(
      this._requestURL(this.url("activities"), this.entityId).toString(),
      {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
          ...(this._lti?.headers ?? { "X-CSRF-Token": this.get("token") }),
        },
        body: JSON.stringify(entity),
      },
    );
    if (!response.ok) {
      let message = `erreur ${response.status}.`;
      switch (response.status) {
        case 403:
          message = `vous n'avez pas l'autorisation d'éditer l'entité ${this.entityId}.`;
          break;
        case 404:
          message = `l'entité ${this.entityId} n'existe pas.`;
          break;
      }
      throw new CapytaleException(
        `Sauvegarde impossible : ${message}`,
        response.status,
      );
    }
  }

  /**
   * Save the activity content to file (network request).
   */
  private async _saveContentFile(content: string) {
    const response = await fetch(
      this.url("file-content").replace(
        "{nid}",
        this.entityId?.toString() as string,
      ),
      {
        method: "PUT",
        headers: {
          "Content-Type": "text/plain",
          ...(this._lti?.headers ?? { "X-CSRF-Token": this.get("token") }),
        },
        body: content,
      },
    );
    if (!response.ok) {
      let message = `erreur ${response.status}.`;
      switch (response.status) {
        case 400:
          message = "la requête de sauvegarde est mal formée ou incomplète.";
          break;
        case 403:
          message = `vous n'avez pas l'autorisation d'éditer le contenu de l'entité ${this.entityId}.`;
          break;
        case 404:
          message = `le contenu de l'entité ${this.entityId} n'existe pas.`;
          break;
        case 413:
          message = "la taille du contenu dépasse la limite autorisée.";
          break;
      }
      throw new CapytaleException(
        `Sauvegarde impossible : ${message}`,
        response.status,
      );
    }
  }

  /**
   * Try the request function until it succeeded
   * (network reconnection + error message).
   */
  private async _sendRequest(request: Function) {
    // sending request
    try {
      await request();
    } catch (error: any) {
      // save failed, log this then try to figure out why
      console.log(error);
      // not exists or too big: no need to try again
      if ([404, 413].includes(error.data)) throw error;
      // in case of malformed request (truncated body?), no need to reload token etc.
      if (error.data !== 400) {
        // does user changed?
        let uid;
        try {
          uid = await this._loadRawUserEntity();
          uid = uid.value;
        } catch (error) {
          console.log(error);
          throw new CapytaleException("Vous n'êtes plus connecté à Capytale.");
        }
        if (uid !== this.get("me"))
          throw new CapytaleException(
            "L'utilisateur a changé et n'a pas les droits sur cette activité.",
          );
        /* here:
                   1) I am connected
                   2) I am the correct user
                   so error should comes from an invalid token so asking for a new
                */
        if (this._lti == null) {
          const token = await this._loadRawToken();
          this.set("token", token["value"]);
        }
      }
      // retrying to save
      try {
        await request();
      } catch (error: any) {
        console.log(error);
        throw new CapytaleException(
          `La requête de sauvegarde a échoué malgré plusieurs tentatives.\n${
            (error as Error)?.message ?? error.toString()
          }`,
        );
      }
    }
  }
}
