# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

from pathlib import Path
import json
import sys
import tarfile


# -- Path setup --------------------------------------------------------------
project_root = Path("..") / ".."
# extract basthon module inside the build dir
site_packages = Path("..") / "build" / "site-packages"
with tarfile.open(
    project_root
    / "node_modules"
    / "@basthon"
    / "kernel-python3"
    / "lib"
    / "dist"
    / "basthon.tar"
) as tar:
    tar.extractall(site_packages)

paths = {
    "capytale": project_root / "packages" / "python-module" / "src",
    "site-packages": site_packages,
}

for path in paths.values():
    sys.path.insert(0, str(path.resolve()))


# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
project = "Basthon-Capytale"
copyright = "2023, Capytale"
author = "L'équipe Capytale"

# The full version, including alpha/beta/rc tags
with open(Path("..") / ".." / "lerna.json") as j:
    lerna = json.load(j)
release = lerna["version"]

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.mathjax",
    "sphinx_autodoc_typehints",
    "myst_parser",
]

autodoc_mock_imports = ["js"]
autoclass_content = "both"
autodoc_member_order = "bysource"

templates_path = ["_templates"]
exclude_patterns = []

language = "fr"

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_book_theme"
html_static_path = ["_static"]
html_logo = "_static/logo.svg"
pygments_style = "default"
