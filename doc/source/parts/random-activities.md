(random-activities)=

# Générer des exercices aléatoires

Il est souvent ludique d'utiliser le module {mod}`random` de Python avec les élèves
car il permet d'écrire des programmes aux résultats imprévus.
Nous autres, enseignants, nous pouvons aussi l'utiliser pour générer aléatoirement
des exercices mais ce n'est pas vraiment satisfaisants. En effet, si l'élève
recharge son activité, l'exercice sera différent et il sera difficile de lui apporter
une correction individualisée.

Le module {mod}`capytale.random` permet de contourner ceci. Sa méthode
{func}`user_seed <capytale.random.user_seed>`
permet de générer une **graine** propre à chaque utilisateur. Il faut alors fournir cette
graine au module {mod}`random` de Python pour profiter de la magie :

```python
from capytale.random import user_seed
import random

random.seed(a=user_seed())

# donnera toujours le même nombre pour cet utilisateur,
# même dans une autre activité !
random.randint(0, 99)
```

Pour une documentation détaillée du module {mod}`random <capytale.random>` de Capytale,
consulter {mod}`cette section <capytale.random>`.

## Un exemple d'activité

```{note}
L'activité présentée ici n'est probablement pas réaliste mais sa
simplicité permet d'insiter sur la technique mise en œuvre.
```

Ainsi, on peut proposer le notebook ci-dessous aux élèves.

---

> **Exercice :** Validez la cellule ci-dessous pour afficher la consigne de l'exercice.

```python
from _exercices import *

consigne_exercice1()
```

> **Exercice :** dans la cellule ci-dessous,
> implémenter en Python la fonction {math}`f` définie par {math}`f(x) = -3x + 7`.

```python
def f(x):
    ...
```

---

Dans lequel le contenu du module `_exercices.py` pourrait être celui-ci :

```python
# contenu de _exercices.py
import random
from capytale.random import user_seed
from IPython.display import Markdown


def consigne_exercice1():
    random.seed(a=user_seed())
    a = random.randint(-15, -2)
    b = random.randint(1, 15)
    return Markdown(f"> **Exercice :** dans la cellule ci-dessous, "
        "implémenter en Python la fonction $f$ définie par $f(x) = {a}x + {b}$.")
```

## Correction de l'activité

Dans une autre activité, on pourra proposer la correction de cet exercice,
par exemple comme ceci.

---

> **Exercice :** Validez la cellule ci-dessous pour afficher la correction de l'exercice.

```python
from _exercices import *

correction_exercice1()
```

> Il fallait implémenter en Python la fonction {math}`f` définie par {math}`f(x) = -3x + 7`.
> On peut faire comme ceci :

```python
def f(x):
    return -3 * x + 7
```

---

````python
# contenu de _exercices.py
import random
from capytale.random import user_seed
from IPython.display import Markdown


def correction_exercice1():
    # l'initialisation puis les appels au générateur aléatoire
    # exactements identiques garantissent que les mêmes valeurs
    # sont générées
    random.seed(a=user_seed())
    a = random.randint(-15, -2)
    b = random.randint(1, 15)
    return Markdown(f"""\
> Il fallait implémenter en Python la fonction $f$ définie par $f(x) = {a}x + {b}$.
> On peut faire comme ceci :

```python
def f(x):
    return {a} * x + {b}
```
""")
````

Ainsi, même si les activités sont différentes, les élèves retrouverons la
correction correspondant à leur exercice.

```{tip}
Il peut être intéressant de combiner cette fonctionnalité avec celle de
{ref}`l'auto-évaluation <auto-evaluation>` afin de proposer à l'élève
une expérience unique et autonome.
```

```{hint}
La méthode `user_module` tient compte du mode de consultation de l'activité.
En effet, si c'est l'enseignant qui consulte l'activité de l'élève, la graine
retournée sera celle de l'élève.
```
