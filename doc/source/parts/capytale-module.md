(capytale-module)=

# API du module `capytale`

```{eval-rst}
.. automodule:: capytale
    :members:
```

## `capytale.random`

```{eval-rst}
.. automodule:: capytale.random
    :members:
```

## `capytale.autoeval`

The module {mod}`autoeval <capytale.autoeval>` is often use in the context of a
{ref}`sequenced notebook <sequenced-notebook>` but this is not mandatory.

```{eval-rst}
.. automodule:: capytale.autoeval
   :show-inheritance:
   :members: validationclass, Validate, equality, ValidateSequences

.. autoclass:: ValidateVariables
   :show-inheritance:
.. autoclass:: ValidateFunction
   :show-inheritance:
.. autoclass:: ValidateFunctionPretty
   :show-inheritance:
```
