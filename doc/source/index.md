# Basthon dans Capytale

Dans **Capytale**, certaines activités utilisent un environement appelé [Basthon](https://basthon.fr).
Il s'agit des activités **Python Notebook**, **Python Script-Console**, **SQL** et **OCaml**. Cette documentation
aborde les aspects spécifiques à l'utilisation de Basthon dans Capytale.

```{note}
Basthon est un environement à part entière, qui existe en dehors de Capytale.
Sa [documentation est disponible ici](https://basthon.fr/doc/)
```

L'utilisation de Basthon dans Capytale améliore l'expérience utilisateur car Capytale
permet un échange fluide entre enseignant et élèves mais aussi parce que des fonctionnalités
spécifiques à Capytale sont disponibles dans Basthon.

Par exemple, dans cette documentation, vous apprendrez comment :

- {ref}`générer des exercices aléatoires mais reproductibles avec une correction cohérente <random-activities>` ;
- {ref}`utiliser l'auto-évaluation avec les élèves <auto-evaluation>`, ou encore
- {ref}`bien prendre en main le notebook séquencé <sequenced-notebook>`.

```{warning}
Même si nous limitons au maximum la gêne des utilisateurs, Basthon et Capytale sont activement développés.
Les API peuvent changer fréquement.
```

# Utilisation avancée de Python dans Capytale

Les pages ci-dessous documentent l'utilisation avancée de Python dans Capytale
ainsi que l'API du module `capytale`.

```{toctree}
:caption: Python dans Capytale
:maxdepth: 2

parts/auto-evaluation
parts/random-activities
parts/sequenced-notebook
parts/capytale-module
```
